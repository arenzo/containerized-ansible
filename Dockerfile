FROM alpine:latest

RUN apk add python3 ansible

WORKDIR /ansible

#Set this so that Ansible doesn't prompt you to accept host keys everytime it runs
ENV ANSIBLE_HOST_KEY_CHECKING False

ENTRYPOINT ["ansible"]
